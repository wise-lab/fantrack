FANTrack: 3D Multi-Object Tracking with Feature Association Network
========================================================================
Source code is available in public_release branch

Requirements
========================================================================

1. numpy 
2. tensorflow-gpu 1.12 
3. scipy 
4. opencv-python 
5. matplotlib
7. scikit-learn 
8. filterpy
9. tqdm




![](qual1.jpg)